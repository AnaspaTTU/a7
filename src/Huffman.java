
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

//Kasutatud materjalid
// https://docs.oracle.com/javase/8/docs/api/java/util/stream/Stream.html
// https://www.geeksforgeeks.org/stream-sorted-in-java/
// https://docs.oracle.com/javase/8/docs/api/?java/math/BigInteger.html
// https://www.tutorialspoint.com/parsing-and-formatting-a-byte-array-into-binary-in-java

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {

    private Node root = null;
    private Map<Byte, Integer> frequencyOfChars = new HashMap<>();
    private List<Node> nodes = new ArrayList<>();
    private Map<Byte, String> codeAsString = new HashMap<>();
    private int encodedLength = 0;

    /**
     * Constructor to build the Huffman code for a given bytearray.
     *
     * @param original source data
     */
    Huffman(byte[] original) {
        if (original == null) {
			throw new NullPointerException("Empty input in Huffman()");
		}
        countSymbols(original);
        createLeaves();
        createTree();
    }

    /**
     * Length of encoded data in bits.
     *
     * @return number of bits
     */
    public int bitLength() {
        return this.encodedLength;
    }

    /**
     * Encoding the byte array using this prefixcode.
     *
     * @param origData original data
     * @return encoded data
     */
    public byte[] encode(byte[] origData) {
        if (origData == null) {
            throw new NullPointerException("Null can not be encoded!");
        }

        StringBuilder builder = new StringBuilder();
        if (frequencyOfChars.keySet().size() == 1) {
            for (int i = 0; i < origData.length; i++) {
                builder.append("1");
            }
            encodedLength = builder.length();
            return new BigInteger(1 + builder.toString(), 2).toByteArray();
        }

        for (byte b : origData) {

            StringBuilder sb = new StringBuilder();
            if (!this.codeAsString.containsKey(b)) {
                Node n;
                if (root.getLeftLeaves().contains(b)) {
                    n = root.getLeftChild();
                    sb.append("0");
                } else {
                    n = root.getRightChild();
                    sb.append("1");
                }

                while (!n.isLeaf()) {
                    if (n.getLeftLeaves().contains(b)) {
                        n = n.getLeftChild();
                        sb.append("0");
                    } else {
                        n = n.getRightChild();
                        sb.append("1");
                    }
                }
                builder.append(sb);
                this.codeAsString.put(b, sb.toString());
            } else {
                builder.append(this.codeAsString.get(b));
            }
        }

        encodedLength = builder.length();
        return new BigInteger(1 + builder.toString(), 2).toByteArray();
    }

    /**
     * Decoding the byte array using this prefixcode.
     *
     * @param encodedData encoded data
     * @return decoded data (hopefully identical to original)
     */
    public byte[] decode(byte[] encodedData) {
        if (encodedData == null) {
            throw new NullPointerException("Null can not be decoded!");
        }


        BigInteger binary = new BigInteger(encodedData);
        String inputAsString = binary.toString(2);
        inputAsString = inputAsString.substring(1);

        if (!inputAsString.contains("0")){
            byte[] result = new byte[inputAsString.length()];
            int count = 0;
            while( count < result.length ) {
                result[count] = root.getCode();
                count++;
            }
            return result;
        }

        Node n = this.root;
        List<Byte> resultList = new LinkedList<>();
        char[] charArray = inputAsString.toCharArray();
        for (char c : charArray) {
            if ( c == '0') {
                n = n.getLeftChild();
            }
            if ( c == '1') {
                n = n.getRightChild();
            }
            if (n.isLeaf()) {
                resultList.add(n.getCode());
                n = this.root;
            }
        }

        byte[] resultArray = new byte[resultList.size()];
        for (int i = 0; i < resultArray.length; i++) {
            resultArray[i] = resultList.get(i);
        }

        return resultArray;
    }

    /**
     * Count symbol appearance in original input
     *
     * @param input byte array
     */
    public void countSymbols(byte[] input) {
        if (input == null) {
            throw new NullPointerException("Empty input");
        }

        for (byte i : input) {
            frequencyOfChars.put(i, frequencyOfChars.getOrDefault(i, 0) + 1);
        }
    }

    /**
     * Create tree
     */
    public void createTree() {
        while (!nodes.isEmpty()) {
            nodes = nodes.stream()
                    .sorted(Comparator.comparing(a -> a.getFrequency()))
                    .collect(Collectors.toList());

            if(nodes.size() == 1) {
                this.root = nodes.get(0);
                nodes.clear();
                break;
            }

            Node left = nodes.get(1);
            Node right = nodes.get(0);
            nodes.remove(left);
            nodes.remove(right);
            int f = left.getFrequency() + right.getFrequency();
            Node parent = new Node(f, left, right);
            parent.addLeftLeaves();
            parent.addRightLeaves();
            nodes.add(parent);
        }

    }

    /**
     * Create leaves of the tree
     */
    public void createLeaves() {
        for (byte b : this.frequencyOfChars.keySet()) {
            Node node = new Node(b, this.frequencyOfChars.get(b));
            this.nodes.add(node);
        }
    }

    /**
     * Node of binary tree
     * Each node has two pointers:
     * 1. leftChild points left child of this node
     * 2. rightChild points right child of this node
     */
    class Node {
        private byte code;
        private int frequency;
        private Node rightChild;
        private Node leftChild;
        private Map<String, Set<Byte>> leaves = new HashMap<>();

        /**
         * Node constructor
         *
         * @param f frequency of Symbol in original input
         * @param l left child
         * @param r right child
         */
        Node(int f, Node l, Node r) {
            setFrequency(f);
            setLeftChild(l);
            setRightChild(r);
        }

        /**
         * Node constructor
         *
         * @param c code of node
         * @param f frequency of Symbol in original input
         *          Node left child is null
         *          Node right child is null
         */
        Node(Byte c, int f) {
            setCode(c);
            setFrequency(f);
            setLeftChild(null);
            setRightChild(null);
        }

        /**
         * Set code of the node
         *
         * @param c code
         */
        public void setCode(Byte c) {
            this.code = c;
        }

        /**
         * returns code of the node
         *
         * @return code
         */
        public Byte getCode() {
            return this.code;
        }

        /**
         * Set frequency of the node
         *
         * @param i frequency
         */
        public void setFrequency(int i) {
            this.frequency = i;
        }

        /**
         * Returns frequency of the node
         *
         * @return frequency
         */
        public int getFrequency() {
            return this.frequency;
        }

        /**
         * Set left child of the node
         *
         * @param l left child
         */
        public void setLeftChild(Node l) {
            this.leftChild = l;
        }

        /**
         * Set right child of the node
         *
         * @param r right child
         */
        public void setRightChild(Node r) {
            this.rightChild = r;
        }

        /**
         * Returns left child of the Node
         *
         * @return left child
         */
        public Node getLeftChild() {
            return this.leftChild;
        }

        /**
         * Returns right child of the Node
         *
         * @return right child
         */
        public Node getRightChild() {
            return this.rightChild;
        }

        /**
         * Checks if the node has left child
         *
         * @return true if node has left child, if not - false
         */
        public boolean hasLeftChild() {
            return this.getLeftChild() != null;
        }

        /**
         * Checks if the node has right child
         *
         * @return true if node has right child, if not - false
         */
        public boolean hasRightChild() {
            return this.getRightChild() != null;
        }

        /**
         * Checks if the node is a leaf
         *
         * @return true if node does not have right and left children, otherwise false
         */
        public boolean isLeaf() {
            return (!hasLeftChild() && !hasRightChild());
        }

        /**
         * Return set of bytes which represents leaves codes of right children node (subtree)
         * @return set of Bytes
         */
        public Set<Byte> getRightLeaves() {
            return this.leaves.get("r");
        }

        /**
         * Return set of bytes which represents leaves codes of left children node (subtree)
         * @return set of Bytes
         */
        public Set<Byte> getLeftLeaves() {
            return this.leaves.get("l");
        }

        /**
         * Add all leaves codes of right subtree
         */
        public void addRightLeaves() {
            if (!this.isLeaf()) {
                Set<Byte> b = new HashSet<>();
                if ( !this.getRightChild().isLeaf()) {
                    b.addAll(this.getRightChild().getRightLeaves());
                    b.addAll(this.getRightChild().getLeftLeaves());
                } else {
                    b.add(this.getRightChild().getCode());
                }
                this.leaves.put("r", b);
            }
        }

        /**
         * Add all leaves codes of left subtree
         */
        public void addLeftLeaves() {
            if (!this.isLeaf()) {
                Set<Byte> b = new HashSet<>();
                if ( !this.getLeftChild().isLeaf()) {
                    b.addAll(this.getLeftChild().getLeftLeaves());
                    b.addAll(this.getLeftChild().getRightLeaves());
                } else {
                    b.add(this.getLeftChild().getCode());
                }
                this.leaves.put("l", b);
            }
        }
    }

    /**
     * Main method.
     */
    public static void main(String[] params) {
//        String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
//        String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEFFFFFF";
//        String tekst = "AB";
//        String tekst = "A";
        String tekst = "AAAAABC";
        byte[] orig = tekst.getBytes();
        Huffman huf = new Huffman(orig);
        byte[] kood = huf.encode(orig);
        byte[] orig2 = huf.decode(kood);
        // must be equal: orig, orig2
        System.out.println(Arrays.equals(orig, orig2));
        int lngth = huf.bitLength();
        System.out.println("Length of encoded data in bits: " + lngth);
    }
}

